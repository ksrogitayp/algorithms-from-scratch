#include <stdlib.h>
#include <stdio.h>
#include <math.h>


struct Term 
{
    int coef;
    int exp;
};

struct Polynomial
{
    int size;
    struct Term* t;
};

void display(struct Polynomial polynomial) {

    printf("x = ");
    for (int i = 0; i < polynomial.size ; i++)
    {
        printf("%d*x^%d", polynomial.t[i].coef, polynomial.t[i].exp);
        if (i != polynomial.size - 1)
        {
            printf(" + ");
        }
        else
        {
            printf(";\n");
        }
    }
}

void create(struct Polynomial* polynomial) {
    printf("Number of terms: \n");
    scanf("%d", &polynomial->size);

    polynomial->t = (struct Term*) malloc(polynomial->size * sizeof(struct Term));

    for ( int i = 0; i < polynomial->size; i++)
    {
        printf("Enter coefficient and expanent\n");
        scanf("%d %d", &polynomial->t[i].coef,  &polynomial->t[i].exp);
    }
}

int evaluate(struct Polynomial* pol) {
    printf("Enter value of x = ");
    int x;
    scanf("%d", x);
    int result;
    for (int i = 0; i < pol->size; i++)
    {   
        result += pol->t[i].coef * pow(x, pol->t[i].exp);
    }
    return result;
}

struct Polynomial* add(struct Polynomial one, struct Polynomial two) {

    struct Polynomial* result;
    result = (struct Polynomial*) malloc(sizeof(struct Polynomial));
    result->t = (struct Term*) malloc((one.size + two.size) * sizeof(struct Term));

    int i, j, k;
    i = j = k = 0;

    while (i < one.size && j < two.size)
    {
        if (one.t[i].exp > two.t[j].exp)
        {
            result->t[k++] = one.t[i++];
        } else if (one.t[i].exp < two.t[j].exp)
        {
             result->t[k++] = two.t[j++];
        } else
        {
            result->t[k].exp = one.t[i].exp;
            result->t[k++].coef = one.t[i++].coef + two.t[j++].coef;
        }
    }

    while (i < one.size)
    {
       result->t[k++] = one.t[i++];
    }
    
    while (j < one.size)
    {
        result->t[k++] = two.t[j++];
    }

    result->size = k;
    return result;
    
};

int main() {
    struct Polynomial one;
    struct Polynomial two;
    struct Polynomial* sum;

    create(&one);
    create(&two);

    sum = add(one, two);

    printf("\n");
    display(one);
    printf("\n");
    
    display(two);
    printf("\n");

    display(*sum);
    printf("\n");
    return 0;
}