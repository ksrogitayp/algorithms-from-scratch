#include <iostream>

class Diagonal {
private:
    int* array;
    int size;

public:
    Diagonal(int s) {
        size = s;
        array = new int[s];
    }

    void fillMatrix();
    void addElement(int i, int j, int element);
    int getElement(int i, int j);
    void print();
};

void Diagonal::fillMatrix() {
    int val;
    for (int i = 0; i < size; i++)
    {       
            std::cout << "Enter an element for x = "<< i << " and y = " << i << "\n";
            std::cin >> array[i]; 
    }
    
}

void Diagonal::addElement(int i, int j, int element) {
    if (i == j) 
    {
        array[i] = element;
    }
}

int Diagonal::getElement(int i, int j) {
    if (i == j)
    {
        return array[i];
    }
    else
    {
        return 0;
    }
}

void Diagonal::print() {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            std::cout << getElement(i, j) << " ";
        }
        std::cout << "\n";
    }
}

void showMenu() {
    std::cout << "Menu:\n" 
        << "1. Fill\n"
        << "2. Print\n"
        << "3. Add element\n"
        << "4. Get element\n"
        << "99. exit\n";  
}

int main() {

    std::cout << "Enter a capacity of matrix\n";
    int s;
    std::cin >> s;
    Diagonal matrix = Diagonal(s);
    
    int option;
    showMenu();
    std::cin >> option;

    int i, j, val;
    while (option != 99) 
    {
        switch (option)
        {
        case 1:
            matrix.fillMatrix();
            break;
        case 2:
            matrix.print();
            break;
        case 3:
            std::cout << "Enter row number: \n";
            std::cin >> i;
            std::cout << "Enter column number: \n";
            std::cin >> j;
            std::cout << "Enter an element: \n";
            std::cin >> val;
            if (i != j)
            {
                matrix.addElement(i, j, 0);
            }
            else
            {
                matrix.addElement(i, j, val);
            }
            
            break;

        case 4:
            std::cout << "Enter row number: \n";
            std::cin >> i;
            std::cout << "Enter column number: \n";
            std::cin >> j;
            std::cout << matrix.getElement(i, j) << "\n";
            break;
        default:
            break;
        }
        showMenu();
        std::cin >> option;
    }
    

    return 0;
}