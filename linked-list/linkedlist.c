#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Node
{
    int data;
    struct Node* next;
};

struct Node* toLinkedList(int* array, int n)
{
    if (n == 0)
    {
        return NULL;
    }

    struct Node* first;
    struct Node* new;
    struct Node* last;

    first = (struct Node*) malloc(sizeof(struct Node));
    first->data = array[0];
    first->next = NULL;
    last = first;

    if (n == 1)
    {
        return first;
    }

    for (int i = 1; i < n; i++)
    {
        new = (struct Node*) malloc(sizeof(struct Node));
        new->data = array[i];
        new->next = NULL;
        last->next = new;
        last = new;
    }

    return first;
}

void display(struct Node* node)
{
    printf("linked list = {");
    while (node != NULL)
    {
        printf("%d", node->data);
        if (node->next != NULL)
        {
            printf(", ");
        }
        node = node->next;
    }
    printf("}\n");
}

int* createArray(int size) {
    int* array = (int*) malloc (sizeof(int) * size);

    for (int i = 0; i < size; i++)
    {
        printf("enter the element #%d: ", i+1);
        scanf("%d", &array[i]);
    }

    return array;
}

int count(struct Node* node) {
    int c = 0;
    while (node != NULL)
    {
        c++;
        node = node->next;
    }
    return c;
}

int sum(struct Node* node) {
    int sum = 0;
    while (node != NULL)
    {
        sum += node->data;
        node = node->next;
    }
    return sum;
}

struct Node* max(struct Node* node) {
	if(node == NULL) {
		return NULL;
	}
	
	struct Node* max = node;
	node = node->next;

	while(node != NULL) {
		max = max->data > node->data ? max : node;
		node = node->next;
	}
	return max;
}

struct Node* min(struct Node* node) {
	if(node == NULL) {
		return NULL;
	}
	
	struct Node* min = node;
	node = node->next;

	while(node != NULL) {
		min = min->data < node->data ? min : node;
		node = node->next;
	}
	return min;
}

struct Node* search(struct Node* node, int key) {
	while(node != NULL) {
		if(key == node->data) {
			return node;
		}
		node = node->next;
	}
	return NULL;
}

struct Node* searchAndMoveToHead(struct Node* node, int key) {
	struct Node* head = node;
	struct Node* previous = NULL;

	while(node != NULL) {
		if(key == node->data) {
			if(previous != NULL) {
				previous->next = node->next;
				node->next = head;
				head = node;
				return head;
			} else {
				return node;
			}
		}
			
		previous = node;
		node = node->next;
	}
	return NULL;
}

struct Node* insertHead(struct Node* head, struct Node* new) {
	if(new == NULL) {
		return head;
	}
	
	if(head == NULL) {
		return new;
	}
	
	new->next = head;
	head = new;
	return new;
}

struct Node* insertAfter(struct Node* node, struct Node* new) {
	if(new == NULL) {
		return node;
	}
	
	if(node == NULL) {
		return new;
	}
	
	new->next = node->next;
	node->next = new;
	return node;
}

struct Node* insertSorted(struct Node* list, int val) {
	struct Node* new = (struct Node*) malloc(sizeof(struct Node));
	new->data = val;
	
	if (list == NULL) {
		return new;
	}

	struct Node* head = list;
	struct Node* prev = NULL;

	while (list != NULL && list->data < val) {
		prev = list;
		list = list->next;
	}

	if (prev == NULL) {
		new->next = head;
		head = new;
	} else if (list == NULL) {
		prev->next = new;
	} else {
		new->next = list;
		prev->next = new;
	}
	
	return head;
}

void deleteNode(struct Node* prev, struct Node* toDelete) {
	prev->next = toDelete->next;	
}

int isSorted(struct Node* list) {
	
	if(list == NULL) {
		return 1;
	}
	
	// find direction

	int first = list->data;
	list = list->next;

	if(list == NULL) {
		return 1;
	}

	int second = list->data;
	int prev = second;
	list = list->next;

	if (first < second) {
		
		// from smaller to bigger
		while(list != NULL) {
		if (list->data < prev) {
			return 0;
		}
		prev = list->data;
		list = list->next;
		}

	} else {
		
		//reversed
		while(list != NULL) {
		if (list->data > prev) {
			return 0;
		}
		prev = list->data;
		list = list->next;
		}

	}
	
	return 1;
}

void removeDuplicatesFromSorted(struct Node* list) {
	
	if(list == NULL) {
		return;
	}
	struct Node* prev = list;
	list = list->next;

	while(list != NULL) {
		if (list->data == prev->data) {
			deleteNode(prev, list);
		} else {
			prev = list;
		}

		list = list->next;
	}
}

struct Node* reverse(struct Node* list) {
	
	struct Node* copy = NULL;
	struct Node* prev = NULL;

	while(list != NULL) {
		copy = prev;
		prev = list;
		list = list->next;
		prev->next = copy;
	}

	return prev;
}

int main() {
	
	int option;
	struct Node* list = NULL; 	
	
	while(option != 99) {
		printf("Select action:\n");
		printf("1 - create linked list from array\n");
		printf("2 - create sorted linked list\n");
		printf("3 - add new node to sorted linked list\n");
		printf("4 - display linked list\n");
		printf("5 - check is linked list sorted\n");
		printf("6 - remove duplicates from sorted list\n");
		printf("7 - reverse linked list\n");
		printf("99 - exit\n");
		scanf("%d", &option);

		switch(option) {
	
			case 1:
				{
					printf("enter size of array: \n");
					int size;
					scanf("%d", &size);
			
					int* array = createArray(size);
					list = toLinkedList(array, size);
					break;
				}

			case 2:
				{
					list = NULL;

					printf("enter number of nodes: \n");
					int size;
					scanf("%d", &size);
					
					int val;
					for (int i = 0; i < size; i++) {
						printf("enter value of the %d node: ", i+1);
        					scanf("%d", &val);
						
						list = insertSorted(list, val);	
					}
				
					break;
				}

			case 3: 
				{
					int val;
					printf("enter value of the node: \n");
        				scanf("%d", &val);
					
					list = insertSorted(list, val);	

				}
			case 4:
				if (list == NULL) {
					printf("Linked list isn't created. Try options 1 or 2 before display\n");
					break;
				}
				display(list);
				break;
			case 5: 
				{
					int isListSorted = isSorted(list);

					if(isListSorted) {
						printf("Linked list sorted.\n");
					} else {
						printf("Linked list not sorted\n");
					}
					break;
				}
				
			case 6: 
				{
 					removeDuplicatesFromSorted(list);
					break;
				}
			case 7: 
				{	
					list = reverse(list);
					break;
				}
				
			defaut:
				break;
		}	
	}
	return 0;
}
